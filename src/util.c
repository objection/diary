#define _GNU_SOURCE

#include "all.h"
#include "util.h"
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

// Maybe this should return an enum and deal with
// more. 2023-03-13T13:19:24+00:00: really this function shouldn't
// exist. What happens if some weird error happens? You'd need
// to check errno anyway.
nod
bool fexists_and_is (char *fpath, mode_t mode) {
	struct stat statbuf;
	int rt = stat (fpath, &statbuf);
	if (rt || (statbuf.st_mode & mode) == mode)
		return 1;
	return 0;
}

// I'm not sure if this function is a good idea. Surely if HOME
// is fucked, something serious is wrong with the system?
// Gets $HOME. If it doesn't find it, tries to construct it from
// LOGNAME.
// If you pass a buffer, it will use that, else it will allocate it.
// Regardless, it passes a pointer to the data: buf if buf, else res.
// I'm not sure if that's a good thing to do.
// Glib tries something to do with passwd but I won't.
nod
static char *get_home_dir (char *buf, size_t nbuf) {
	char *r = getenv ("HOME");
	size_t ret = 0;
	if (!r) {
		char *logname = getenv ("LOGNAME");
		if (!logname)
			return 0;
		else {
			if (!buf)
				asprintf (&r, "/home/%s", logname);
			else {
				ret = snprintf (buf, nbuf, "/home/%s", logname);
				if (ret == -1)
					return 0;
			}
		}
	} else {
		if (!buf)
			r = strdup (r);
		else
			strcpy (buf, r);
	}
	return buf? buf: r;
}

// Gets XDG_DATA_HOME. If it isn't defined (I think it's meant
// to be defined by the user), use default, which is ~/.local/share.
// If buf, use that, else return res.
// If buf is 0, allocates into res, else copies into buf.
nod
char *get_xdg_data_home (char *buf, size_t nbuf) {
	char *r = getenv ("XDG_DATA_HOME");
	bool return_buf = 0;
	char scratch[PATH_MAX];
	if (!r) {
		char *home = get_home_dir (scratch, PATH_MAX);
		if (!home)
			return 0;
		else {
			if (!buf)
				asprintf (&r, "%s/.local/share", home);
			else {
				int ret = snprintf (buf, PATH_MAX, "%s/.local/share", home);
				if (ret && ret >= nbuf || ret < 0)
					return 0;
				return_buf = 1;
			}
		}
	} else {
		if (buf) {
			strcpy (buf, r);
			return_buf = 1;
		} else
			r = strdup (r);
	}
	return return_buf ? buf : r;
}

// Never matches '.' or '..'
void ls_matching_mode_t (char *dir, mode_t mode,
		bool print_full_path, bool ignore_dotfiles) {
	DIR *dp = opendir (dir);
	if (!dp) {
		fprintf (stderr, "opendir failed: %s\n", strerror (errno));
		exit (1);
	}
	struct dirent *dirent;
	char fpath[PATH_MAX + 1];
	size_t ndir = strlen (dir);
	while ((dirent = readdir (dp))) {
		if (strcmp (dirent->d_name, ".") != 0 &&
				strcmp (dirent->d_name, "..") != 0 &&
				!(ignore_dotfiles && *dirent->d_name == '.')) {
			if (print_full_path) {
				if (strlen (dirent->d_name) + ndir > PATH_MAX) {
					fprintf (stderr, "path is bigger than PATH_MAX (%d)\n",
							PATH_MAX);
					exit (1);
				}
				sprintf (fpath, "%s/%s", dir, dirent->d_name);
				struct stat statbuf;
				stat (fpath, &statbuf);
				if (statbuf.st_mode & mode)
					printf ("%s\n", fpath);
			} else
				printf ("%s\n", dirent->d_name);
		}
	}
	closedir (dp);
}

nod enum derr mk_dir (cchar *path, mode_t mode) {
	if (mkdir (path, mode)) {
		if (errno == EEXIST)
			return SUCC;
		WARN_RETURN (FAIL, "Couldn't make dir %s: %m", path);
	}
	return SUCC;
}

nod enum derr ch_dir (cchar *path) {
	if (chdir (path))
		WARN_RETURN (FAIL, "Couldn't chdir to %s: %m", path);
	return SUCC;
}

char *fmt (char *buf, size_t n_buf, char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);
	if (!buf)
		assert (vasprintf (&buf, fmt, ap) >= 0);
	else
		assert (vsprintf (buf, fmt, ap) >= 0);
	va_end (ap);
	return buf;
}

