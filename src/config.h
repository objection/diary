// What I mean by this is the basname of the directory diary
// will make and deal with. In total you're "user-wide" diary
// might be ~/.local/share/diary.
#define DATA_DIR_NAME "diary"
#define DEFAULT_DIR_PERMISSIONS 0770
#define DEFAULT_DIARY "diary"

// Define if you want to prefix the archive dirs with a string eg
// "archive_", making eg "archive_19".
#define ARCHIVE_DIR_NAME_PREFIX ""

// Change if you want your files to have a different suffix, eg .md.
// Also you can use set it to "" to not have one. You can also do this
// in the config files (really, that's how you should do it), with
// "file-suffix".
#define FILE_SUFFIX ".txt"

// Basically don't touch this, because it's used to name the files
// and the filenames are used to archive the entries. If you do want
// to change it, you can't just change this; you'll have the change
// the time_str functions. Use strptime.
#define TIME_SEP "_"
#define DATE_FORMAT "%y-%m-%dT%H" TIME_SEP "%M" TIME_SEP "%S"
#define D "[[:digit:]][[:digit:]]"
#define DATE_GLOB D "-" D "-" D "T" D TIME_SEP D TIME_SEP D

// Default archive time, in seconds. There's other TS_SECS_FROM ...
// macros in time_string.h.
#define DEFAULT_WHEN_ARCHIVE TS_SECS_FROM_DAYS(1)

// Whether to put the date into files or not
// #define DEFAULT_COMMAND "printf \"%%s\n\n\n\" `date -Iseconds` > %s"
#define CONFIG_FNAME "config.toml"

