#define _GNU_SOURCE

#include "all.h"
#include "util.h"
#include "toml.h"
#include "lib/time-string/time-string.h"
#include "lib/readlinef/readlinef.h"
#include "lib/n-fs/n-fs.h"
#include "lib/rab/rab.h"
#include <stdarg.h>

#define USER_VARS($x) \
	$x (UV_FILE_SUFFIX,              "file-suffix") \
	$x (UV_DEFAULT_DIARY,            "default-diary") \
	$x (UV_ARCHIVE,            	     "archive") \
	$x (UV_START,            	   	 "start") \
	$x (UV_INTERPRETER,              "interpreter") \
	$x (UV_CONFLICT_DIFF_AND_REMOVE, "conflict-diff-and-remove-command") \
	$x (UV_NEW_ENTRY,                "new-entry-command") \
	$x (UV_EDIT_ENTRY,               "edit-entry-command") \
	$x (UV_VIEW_ENTRY,               "view-entry-command") \
	$x (UV_PRINT,                    "print-command")

enum cfg_key { USER_VARS(SEL_1) };
static char *cfg_key_strs[] = { USER_VARS(SEL_2) };

static struct settings {
	struct ts_dur when_archive;
	char *new_entry_cmd;
	char *file_suffix;
	char *edit_entry_cmd;
	char *conflict_diff_cmd;
	char *view_entry_cmd;
	char *post_cmd;
	char *print_cmd;
	struct ts_time start;
} settings = {
	.when_archive = {
		.secs = DEFAULT_WHEN_ARCHIVE,
	},
};

#define CHECK_HAVE_EDITOR() \
	({ \
		enum derr r = FAIL; \
		if (!editor) { \
			WARN ("Neither VISUAL nor EDITOR defined. Try prepending VISUAL=<your-editor>"); \
		} else \
	 		r = SUCC; \
		r; \
	})
static char *editor;

// This is a normal enum. You can only do one action
// per run. I don't know if this is the best thing to
// do.
enum action {
	A_DEFAULT,
	A_CAT_FILE,
	A_LIST,
	A_PRINT,
	A_VIEW_FILE,
	A_NEW_DIARY,
	A_NEW_DATA_DIR,
};

enum args_flags {
	AF_NO_ALLOW_INCOMPLETE = 1 << 0,
};

enum print_what {
	P_CURRENT_PATH = 1 << 0,
	P_OLD_PATH     = 1 << 1,
};

struct args {
	enum print_what print_what; // Used with A_PRINT
	enum action action;
	char *start;
	enum args_flags flags;
	char *diary_glob;
	char *data_dir_path;
	char *new_diary;
	char *new_data_dir;
	struct strs interpreter_argv;
};

struct data_dir_settings {
	char *default_diary;
};

struct entry {
	struct ts_time time;
	char *path;
	struct ts_time start;

	// A flag set if the file gets moved. It's for --old-path.
	bool archived;
};
$make_arr (struct entry, entries);

static struct ts_time now;
#define N_SCRATCH PATH_MAX
thread_local char SCRATCH[N_SCRATCH];

#define OPEN_IN_EDITOR(fmt, ...) \
	({ \
	 	enum derr r = CHECK_HAVE_EDITOR (); \
		if (!r) { \
			if ((r = n_fs_systemf (fmt __VA_OPT__(,) __VA_ARGS__))) \
				WARN ("Failed while editing in editor\n"); \
		} \
		r; \
	})

nod
static enum derr get_toml_str_val (toml_table_t *toml_table, char *key, char **out) {
	const char *raw;
	if (raw = toml_raw_in (toml_table, key)) {
		if (toml_rtos (raw, out))
			WARN_RETURN (FAIL, "Bad string. (Put in proper error message)");
	}
	return SUCC;
}

nod
static enum derr set_interpreter (char *arg, struct strs *interpreter_argv) {
	interpreter_argv->n = 0;
	if (!arg) {
		arr_pusharr (interpreter_argv, ((char *[]) {"sh", "-c"}), 2);
		return SUCC;
	}

	wordexp_t p;

	// I'm not really sure if wordexp is the right choice. It
	// does give you tilde expansion, though, and that's
	// useful since this is passes a single string.
	int rc = wordexp (arg, &p,

			// Probably allowing cmds is unsafe?
			WRDE_NOCMD);
	if (rc)
		WARN_RETURN (FAIL, "\"%s\" isn't a proper shell string", arg);

	interpreter_argv->n = 0;
	FORI (i, p.we_wordc)
		arr_add (interpreter_argv, p.we_wordv[i]);
	free (p.we_wordv);
	return SUCC;
}

static void get_time_str_from_filename (char *s) {

	char *_ = strrchr (s, 'T');
	ASSERT (_);
	char *end = strstr (s, settings.file_suffix);
	if (!end || !*settings.file_suffix)
		end = strchr (_, 0);

	for (; _ < end; _++) {
		if (*_ == *TIME_SEP)
			*_ = ':';
	}
}

nod
static enum derr generic_get_settings (toml_table_t *table,
		char *file_path, char toml_err_buf[256], struct args *args) {

	const char *val;
	if (val = toml_raw_in (table, cfg_key_strs[UV_ARCHIVE])) {
		settings.when_archive = ts_dur_from_dur_str (val, 0);
		if (ts_errno)
			WARN_RETURN (FAIL, "Couldn't parse \"%s\" %s: %s", (char *) val,
					cfg_key_strs[UV_ARCHIVE],
					ts_strerror (ts_errno));
	}
	if (val = toml_raw_in (table, cfg_key_strs[UV_START])) {
		char tmp[64];
		strncpy (tmp, val, 64);
		get_time_str_from_filename (tmp);
		settings.start = ts_time_from_str (tmp, 0, &now, TS_MATCH_ONLY_ISO);
		if (ts_errno)
			WARN_RETURN (FAIL, "Parse start time string %s: %s", val,
					ts_strerror (ts_errno));
	}

	if (       get_toml_str_val (table, cfg_key_strs[UV_FILE_SUFFIX], &settings.file_suffix)
			|| get_toml_str_val (table, cfg_key_strs[UV_CONFLICT_DIFF_AND_REMOVE], &settings.conflict_diff_cmd)
			|| get_toml_str_val (table, cfg_key_strs[UV_NEW_ENTRY], &settings.new_entry_cmd)
			|| get_toml_str_val (table, cfg_key_strs[UV_EDIT_ENTRY], &settings.edit_entry_cmd)
			|| get_toml_str_val (table, cfg_key_strs[UV_VIEW_ENTRY], &settings.view_entry_cmd)
			|| get_toml_str_val (table, cfg_key_strs[UV_PRINT], &settings.print_cmd))
		return FAIL;

	char *interpreter = np;
	if (get_toml_str_val (table, cfg_key_strs[UV_INTERPRETER], &interpreter))
		return FAIL;

	if (set_interpreter (interpreter, &args->interpreter_argv))
		return FAIL;
	NUKE_IF (interpreter);
	return SUCC;
}

// Gets settings from the diary's "config" file, which you put in the
// diary's directory, not ~/.config.
nod
static enum derr get_diary_settings (char *diary, struct args *args) {

	char toml_err_buf[256];

	FILE *f = fopen (CONFIG_FNAME, "r");
	if (!f)
		WARN_RETURN (FAIL, "\
Each diary needs a config file called \"" CONFIG_FNAME "\" in its directory");

	toml_table_t *toml_table = toml_parse_file (f, toml_err_buf, 256);
	fclose (f);

	char file_path[PATH_MAX];
	snprintf (file_path, PATH_MAX, "%s/%s/%s", args->data_dir_path, diary, CONFIG_FNAME);

	if (!toml_table)
		WARN_RETURN (FAIL, "Couldn't parse %s: %s", file_path, toml_err_buf);

	if (generic_get_settings (toml_table, file_path, toml_err_buf, args))
		return FAIL;

	toml_free (toml_table);
	return SUCC;
}

nod
static enum derr get_data_dir_settings (struct args *args, struct data_dir_settings *out) {

	char toml_err_buf[256];

	FILE *f = fopen (CONFIG_FNAME, "r");
	if (!f)

		// If you don't have a data_dir_path config, who cares.
		return FAIL;

	toml_table_t *table = toml_parse_file (f, toml_err_buf, 256);
	if (!table)
		WARN_RETURN (FAIL, "Failed to parse %s: %s", CONFIG_FNAME, toml_err_buf);

	char file_path[PATH_MAX];
	snprintf (file_path, PATH_MAX, "%s/%s", args->data_dir_path, CONFIG_FNAME);

	if (get_toml_str_val (table, cfg_key_strs[UV_DEFAULT_DIARY],
				&out->default_diary))
		return FAIL;

	if (generic_get_settings (table, file_path, toml_err_buf, args))
		return FAIL;

	toml_free (table);
	return SUCC;
}

nod
static int cmp_entry (const void *_a, const void *_b) {
	const struct entry *a = _a;
	const struct entry *b = _b;
	return a->time.ts - b->time.ts;
}

nod
static int strcmp_files (char *a_path, char *b_path) {
	char *a_buf = 0;
	char *b_buf = 0;
	int rc = cat (&a_buf, (char *[]) {a_path}, 1, 0);
	ASSERT (!rc);
	rc = cat (&b_buf, (char *[]) {b_path}, 1, 0);
	ASSERT (!rc);
	return strcmp (a_buf, b_buf);
}

nod
static char **get_archived_entry_path (void *user_type, void *) {
	struct entry *entry = user_type;
	if (entry->archived)
		return np;
	return &entry->path;
}

nod
static enum derr run_user_cmd (char *cmd, enum cfg_key cfg_key, struct entry current_entry,
		char *archived_file_path, struct entries *entries, struct args *args) {

	auto pid = fork ();

	switch (pid) {
	case 0:
		arr_add (&args->interpreter_argv, cmd);
		arr_add (&args->interpreter_argv, 0);
		char *archived_entry_paths_env_str = entries && entries->n
			? rab_join (entries->n, entries->d,
					&(rab_join_opts) {
						.getter = &(rab_getter) {
							.func = get_archived_entry_path,
							.type_size = sizeof *entries->d
						},
						.quotes = ","
					})
			: strdup ("");

		// We set these every time, with setenv (..., 1), since
		// sometimes one or other of these is meant to be empty.
		if (current_entry.path)
			setenv ("DIARY_CURRENT_ENTRY", current_entry.path, 1);
		if (archived_file_path)
			setenv ("DIARY_ARCHIVED_ENTRY", archived_file_path, 1);
		if (archived_entry_paths_env_str)
			setenv ("DIARY_ARCHIVED_ENTRIES", archived_entry_paths_env_str, 1);
		execvpe (args->interpreter_argv.d[0], args->interpreter_argv.d, environ);
		WARN ("Couldn't run your %s script", cfg_key_strs[cfg_key]);
		exit (FAIL);
		break;
	case -1:
		WARN_RETURN (FAIL, "Couldn't fork");
		break;
	default:

		int wstatus;
		int rc = waitpid (pid, &wstatus, 0);
		if (rc == -1)
			WARN_RETURN (FAIL, "waitpid failed");
		else if (!WIFEXITED (wstatus))
			WARN_RETURN (FAIL, "Your %s script didn't return in an orderly manner",
					cfg_key_strs[cfg_key]);

		// Here's where we get the exit status of the script. If it
		// returned non-0 we take that as a sign to exit.
		else if (WEXITSTATUS (wstatus))
			WARN_RETURN (FAIL, "Your script returned non-zero (%d)", WEXITSTATUS (wstatus));
		break;
	}
	return SUCC;
}

nod
static enum derr vimdiff_files (char *current_entry_path, char *archived_entry_path) {
	CHECK_HAVE_EDITOR ();
	if (!readlinef_confirm ("\
Run vimdiff on the files, then remove the non-archived file? "))
		return 1;

	fprintf (stdout, "\
Dump everything you want to keep in the second file. After, \
the first will be \"removed\". Actually, it'll be copied to /tmp, in case you mess up\n");
	sleep (1);

	n_fs_systemf ("vimdiff %s %s", current_entry_path, archived_entry_path);
	return 0;
}

// This runs the user (me)'s diff command on the two files, or
// vimdiff if one hasn't been defined.
//
// Then it removes the current file.
//
// FIXME: really, it shouldn't always remove only the current file. If
// somehow the current file -- "todays" file -- has been archived, you
// might prefer to end up with only the current file.
nod
static enum derr diff_and_rm (struct entry *entry, char *archived_path, char *editor,
		struct entries *entries, struct args *args) {

	if (settings.conflict_diff_cmd) {
		if (run_user_cmd (settings.conflict_diff_cmd, UV_CONFLICT_DIFF_AND_REMOVE,
					*entry, archived_path, entries, args))
			return FAIL;
	} else {
		if (!strcmp_files (entry->path, archived_path))
			warnx ("They're identical so we're removing the current one\n");
		else if (vimdiff_files (entry->path, archived_path))
			return FAIL;
		if (n_fs_mv (entry->path, "/tmp", 1))

				// This is pathetic, but this is what we're doing. If
				// n_fs_mv function fails we just shell out.
				//
				// It will fail if entry->path is a directory and entry->path
				// is not on the same filesystem as /tmp. It shouldn't, but I
				// don't want to spend the time making the function work in
				// all cases. I underestimated what a pain it would be.
			n_fs_systemf ("mv %s /tmp", entry->path);
	}

	return SUCC;
}

// Gets all the "current" entries, the ones in, eg, diary/my-diary.
//
// Really, there should only ever be one. But sometimes you do end up
// 		with more.
//
// Reasons for that:
// 		You have the current entry open in an editor somewhere and
// 				save it again after it's been archived.
//
// Is there any way to stop this? No. You have to get the user to deal
// 		with it, which is what happens.
nod
static enum derr get_current_entries (char *diary, struct entries *out) {

	glob_t pglob;
	int ret = glob (fmt (SCRATCH, N_SCRATCH, "%s_" DATE_GLOB "%s",
				diary, settings.file_suffix), 0, 0, &pglob);

	if (ret && ret != GLOB_NOMATCH)
		WARN_RETURN (FAIL, "Glob failed to match a file. It doesn't provide a strerror");

	if (pglob.gl_pathc) {
		arr_reserve (out, pglob.gl_pathc);

		FORI (i, pglob.gl_pathc) {
			if (settings.file_suffix && !strstr (pglob.gl_pathv[i], settings.file_suffix))
				warnx ("\
Your file %s doesn't have your file suffix, %s. Archiving it anyway",
					pglob.gl_pathv[i], settings.file_suffix);

			// This is the basename because were in the diary
			// directlry, eg, diary/diary.
			char *_basename = pglob.gl_pathv[i];
			ASSERT (strlen (_basename) < LEN (SCRATCH) - 1);
			strncpy (SCRATCH, _basename, N_SCRATCH);
			get_time_str_from_filename (SCRATCH);
			struct entry entry = {
				.time.ts = ts_ts_from_str (SCRATCH + strlen (diary) + 1,
						strrchr (SCRATCH, '.'), &now,
						TS_MATCH_ONLY_ISO),

				// FIXME: the path should be relative to the user's
				// cwd, not absolute, in my opinion, but there's no
				// libc function for doing that. And I'm not, today,
				// going to make one, and I'm not going to install a
				// library that can do it.
				.path = realpath (_basename, 0)
			};
			if (ts_errno)
				WARN_RETURN (FAIL, "Couldn't get time from %s: %s",
						entry.path, ts_strerror (ts_errno));
			entry.time.tm = ts_tm_from_ts (entry.time.ts);
			arr_add (out, entry);
		}

		// If there's only one result I only need to free the char **.
		// r.path points to the char *.
		free (pglob.gl_pathv);
	}
	if (out->d)
		qsort (out->d, out->n, sizeof *out->d, cmp_entry);

	return SUCC;
}

nod
static struct entry get_correct_time_entry (void) {
	struct entry r = {
		.time = settings.start,
	};

	// Add settings.when_archive to settings.start until it's past
	// now.
	ts_add_dur_to_time_until (&r.time, now.ts, &settings.when_archive, 0);

	// Then go back one settings.when_archive.
	r.time = ts_time_from_ts (r.time.ts - ts_secs_from_dur (settings.when_archive, &now));
	return r;
}

// Starts at settings.start, adds settings.when_archive to it until
// it's one settings.when_archive less than now.
nod
static enum derr make_new_current_entry (char *diary, struct args *args, struct entry *out) {

	*out = get_correct_time_entry ();

	ts_str_from_ts (SCRATCH, LEN (SCRATCH) - 1, out->time.ts, DATE_FORMAT);
	asprintf (&out->path, "%s/%s/%s_%s%s", args->data_dir_path,
			diary, diary, SCRATCH, settings.file_suffix);
	ASSERT (!n_fs_fexists (out->path));
	if (settings.new_entry_cmd &&
			run_user_cmd (settings.new_entry_cmd, UV_NEW_ENTRY,
				*out, np, np, args))
		return FAIL;
	return SUCC;
}

// Moves the current entries to ,eg, 20, for 2020 if its parsed
// ts + settings.when_archive > now. Also makes the archive dir
// if it doesn't exist. using the remaining if all except one is
// archived. Errors if > 1 remains. That shouldn't happen must be a
// bug.

nod
static enum derr archive_files (struct entries *current_entries,
		char *archive_dir_name, char *diary, size_t n_diary, struct args *args) {

	if (mk_dir (archive_dir_name, DEFAULT_DIR_PERMISSIONS))
		return FAIL;

	EACH (_, current_entries->n, current_entries->d) {
		/* char *current_entry_basename = strrchr (_->path, '/') + 1; */

		// This assumes that settings.when_archive has been set, which
		// it always has, to DEFAULT_WHEN_ARCHIVE, if not set in
		// config.toml.
		//
		// FIXME: this assumes the file has the right ts, which
		// won't always. To be correct it probably should call
		// get_correct_time_entry with a now of &_->time.
		//
		// However, nearly all the time the ts will be correct,
		// because when new entries are made, they're made with the
		// right times.
		if (_->time.ts + ts_secs_from_dur (settings.when_archive, &_->time) < now.ts) {

			int ret = 0;
			sprintf (SCRATCH, "%s/%s", archive_dir_name, basename (_->path));
			if (ret > 255)
				WARN_RETURN (FAIL, "Buffer overflow");
			if (n_fs_fexists (SCRATCH)) {
				if (!isatty (fileno (stdin))) {
					WARN_RETURN (FAIL, "\
Trying to move %s but it already exists in %s. Not outputting to a terminal so \
can't try to diff and remove. The files:\n\
	%s\n\
	%s", _->path, archive_dir_name, _->path,
SCRATCH);
				} else {
					WARN ("Trying to archive %s but it already exists in %s\n",
							_->path, archive_dir_name);
					if (diff_and_rm (_, SCRATCH, editor, 0, args))
						return FAIL;
				}
			} else
				if (rename (_->path, SCRATCH))
					WARN_RETURN (FAIL, "Couldn't move %s to %s", _->path,
							SCRATCH);
			_->archived = 1;
			free (_->path);
			_->path = realpath (SCRATCH, 0);
		}
	}
	return SUCC;
}

nod
static enum derr get_current_entry (struct entries current_entries,
		char *matched_diary, struct args *args, struct entry *out) {

	if (!current_entries.n)

		// If there's no diary entry, make one.
		if (make_new_current_entry (matched_diary, args, out))
			return FAIL;

	int n_got = 0;
	EACH (_, current_entries.n, current_entries.d) {
		if (!_->archived) {

			//
			// Maybe a bit flimsy, but by definition, after
			// archive_files, if there's any non-archived files,
			// there should be only _one_ non-archived file.
			// All older entries will have been archived.
			//
			// This, I suppose, will break in some way if you
			// create two files names that lie within the current
			// day's time range. diary will never do that, but
			// perhaps it's something I could try to guard
			// against.
			//
			// It will also fail, I expect, if somehow you've
			// made a diary whose date describes the future.
			// Again, diary won't do that.
			//
			*out = *_;
			n_got++;
			ASSERT (n_got <= 1);
		}
	}

	if (!n_got && make_new_current_entry (matched_diary, args, out))
		return FAIL;
	return SUCC;
}

nod
static char *match_single_diary_od (char *pattern, bool allow_incomplete_glob,
		char *data_dir_path, struct data_dir_settings *data_dir_settings) {

	if (!pattern)
		WARN_RETURN (np, "\
You have no default diary defined in %s/config.toml and you haven't specified \
one",
data_dir_path);

	// This assumed you've chdired, which you have.
	bool free_pattern = 0;
	if (!pattern)
		pattern = data_dir_settings->default_diary;

	// I match partially-matching patterns: "diary te" will match
	// "test".
	else if (allow_incomplete_glob) {

		// I do this by simply sticking a * at the end.
		if (pattern[strlen (pattern)] != '*') {
			asprintf (&pattern, "%s*", pattern);
			free_pattern = 1;
		}
	}
	char *r = 0;

	glob_t pglob;
	glob (pattern, GLOB_BRACE | GLOB_ONLYDIR, 0, &pglob);

	// I won't bother checking return value of glob. I only care if
	// I've got matches.
	if (pglob.gl_pathc > 1) {
		WARN ("Your glob matched too many diaries: ");
		rab_print_comma_list (pglob.gl_pathc, pglob.gl_pathv,
				&(struct rab_pcl_opts) {.f = stderr});

		return np;
	} else if (pglob.gl_pathc == 1) {
		r = strdup (pglob.gl_pathv[0]);

		if (!fexists_and_is (r, S_IFDIR))
			WARN_RETURN (np, "\"%s\" exists but isn't a directory", r);

	} else
		WARN_RETURN (np, "\"%s\" dir isn't in %s. Create it it with --new",
				pattern, data_dir_path);
	if (free_pattern)
		free (pattern);
	globfree (&pglob);
	return r;
}

nod
static char *get_data_dir (void) {

	char *r = np;
	char user_data_dir[255];

	if (!get_xdg_data_home (user_data_dir, 256))
		WARN_RETURN (np, "Couldn't get your home directory");

	asprintf (&r, "%s/%s", user_data_dir, DATA_DIR_NAME);
	ASSERT (r);
	return r;
}

nod
static enum derr check_add_action (enum action action, struct args *args) {
	if (args->action && args->action != action)
		WARN_RETURN (FAIL, "You're only allowed one action");
	args->action = action;
	return SUCC;
}

nod
static error_t parse_opt (int key, char *arg, struct argp_state *state) {

	struct args *args = state->input;

	switch (key) {
		case ARGP_KEY_INIT:
			break;
		case ARGP_KEY_ARG:
			if (args->diary_glob)
				WARN_RETURN (FAIL, "Only one diary allowed");

			args->diary_glob = arg;

			if (strncmp ("..", arg, 2) || strncmp ("/", arg, 1))
				WARN_RETURN (FAIL, "\
Your diary begins with \"..\" or \"/\": that's not allowed, because that makes \
it a path");

			break;
		case ARGP_KEY_END:
			args->data_dir_path = args->data_dir_path ?: get_data_dir ();

			// strduping so that generic_get_settings can free things
			// and then realloc them.
			if (!settings.file_suffix)
				settings.file_suffix = strdup (FILE_SUFFIX);
			break;
		case 'C':
			if (args->data_dir_path)
				free (args->data_dir_path);

			// FIXME: this feels pretty bad and wrong. I see what I'm
			// doing. I want to turn a "relative" data dir to an
			// absolute one. But diary should just CD to whatever
			// dir you give it and forget about where it is. It
			// doesn't matter, not even for error messages.
			if (strncmp ("..", arg, 2) && !strncmp ("/", arg, 1))
				asprintf (&args->data_dir_path, "%s/%s", getcwd (SCRATCH, PATH_MAX), arg);
			else
				args->data_dir_path = strdup (arg);
			break;
		case 'c':
			if (check_add_action (A_CAT_FILE, args))
				return FAIL;
			break;
		case 'I':
			args->flags |= AF_NO_ALLOW_INCOMPLETE;
			break;
		case 'i':
			if (set_interpreter (arg, &args->interpreter_argv))
				return FAIL;
			break;
		case 'l':
			if (check_add_action (A_LIST, args))
				return FAIL;
			break;
		case 'N':
			if (check_add_action (A_NEW_DATA_DIR, args))
				return FAIL;
			args->new_data_dir = arg;
			break;
		case 'n':
			if (check_add_action (A_NEW_DIARY, args))
				return FAIL;
			args->new_diary = arg;
			break;
		case 'P':
			if (check_add_action (A_PRINT, args))
				return FAIL;
			args->print_what |= P_OLD_PATH;
			break;
		case 'p':
			if (check_add_action (A_PRINT, args))
				return FAIL;
			args->print_what |= P_CURRENT_PATH;
			break;
		case 'v':
			if (check_add_action (A_VIEW_FILE, args))
				return FAIL;
			break;
		default:
			return 0;
			break;
	}
	return 0;
}

nod
static enum derr set_args (int argc, char **argv, struct args *out) {
	return argp_parse (&(struct argp) {
			(struct argp_option []) {
				{"cat", 'c', 0, 0, "Print diary entry to stdout"},
				{"data-dir", 'C', "PATH", 0, "Use PATH as diary data dir. To create on \
instead, use --new-data-dir"},
				{"list", 'l', 0, 0, "List diaries"},
				{"new", 'n', "NAME", 0, "Make a new diary, opening its sensible-defaults config.toml \
file in your editor"},
				{"new-data-dir", 'N', "PATH", 0, "Set up data dir PATH by 1) creating its \
directory; 2) putting config.toml in its root; 3) having you edit it; 4) doing \
what --new does"},
				{"no-incomplete-diary", 'I', 0, 0, "Don't allow incomplete diary globs"},
				{"old-path", 'P', 0, 0, "If running diary archives any \"current\" entries, \
print their old paths, else  print nothing. See README for why you might want this"},
				{"path", 'p', 0, 0, "Print the path of DIARY; don't open. See README for \
why you might want this"},
				{"interpeter", 'i', "CMDLINE", 0, "The cmdline used to run your hooks, eg, \
\"zsh -c\", or \"perl -E\". By default it's \"sh -c\", of course. You can set this \
in your diary's config.toml"},
				{"view", 'v', 0, 0, "Open the entry with the command defined in the diary \
config.toml view-entry-command, or less, if that's not defined"},
			{},
	},
				parse_opt,
				"DIARY",
				"Edit new DIARY entry in EDITOR\v\
Without -C, DIARIES are directories in ~/.local/share/diary if you haven't set \
XDG_DATA_HOME to something else. The CURRENT ENTRY goes there. It's \
named after the \
DIARY and the date, eg \"private_thoughts_18-12-13\" plus your FILE_SUFFIX (by default \
\".txt\".\n\
\n\
DIARIES can be given as globs, eg \"priv\\*\" might match \
\"private_throughts\". You can leave off the final \\*. Use \
--no-incomplete-diary to require it. Globs must expand to only one file.\n\
\n\
Old files are stored in a subdirectory of DIARY whose name is \
ARCHIVE_NAME_DIR_PREFIX followed by the last two digits of the year, eg olds_18.\n\
\n\
Each diary can have a CONFIG FILE (called config.toml), which is kept inside the \
directory of every DIARY, contrary to the usual practice, which is to \
put it in ~/.config/diary. I did this so you can copy around diaries easily. \
What I mean is I kept forgetting to copy the CONFIG FILE.\n\
\n\
When you make your first diary (\"diary --new whatever\") you'll find an \
explanation of what to put there.\n\
\n\
DURATION. A duration is like 4w2d, meaning four weeks, two days. It has to \
be full days"}, argc, argv, 0, 0, out);
}

nod
static enum derr print_diary_cfg_template (const char *diary_name) {
	FILE *f = fopen (CONFIG_FNAME, "w");
	if (!f)
		WARN_RETURN (FAIL, "Can't open %s", CONFIG_FNAME);

	auto start_time = ts_time_from_str ("0dT00:00:00", 0, &now, TS_MATCH_ALL);
	char *start_time_str = ts_str_from_time (SCRATCH, N_SCRATCH, &start_time,
			DATE_FORMAT);

	fprintf (f, "\
# Here's lies default values for diary \"%s\".\n\
#\n\
# \"start\" is necessary so we know when to archive files.\n\
# It's been set here to the start of today. The format is:\n\
#\n\
# 	<year>-<month>-<day-in-month>T<hour>:<minute>:<seconds>.\n\
start = %s\n\
\n\
# \"archive\" is when you want your latest diary entry to be archived. If you cut\n\
# it out the default value is what you've got here, 1 day. Other \"units\" are:\n\
#\n\
# 	y: year\n\
# 	M: month\n\
# 	d: day\n\
# 	h: hour\n\
# 	m: minute\n\
# 	s: second\n\
#\n\
# Examples:\n\
# 	1d5h\n\
# 	1y5M3d15s\n\
		archive = 1d\n\
		\n\
# file-suffix is what it sounds like, the suffix to each diary.\n\
		file-suffix = \".txt\"\n\
#\n\
# You can set up commands to be run at particular point in the\n\
# running of the program. This means you can, with patience,\n\
# make Diary do various things. Below I'll show roughly how\n\
# you could set up what I suppose you might call an audio diary.\n\
#\n\
# You can write scripts directly in the file by using toml's\n\
# multiline string syntax, which is like this:\n\
# \"\"\"\n\
# We're in a multiline\n\
# string\n\
# \"\"\"\n\
#\n\
# Since we CD to the directory this file is in,\n\
# you can also put your scripts in this directory and invoke them\n\
# (\"the-command = ./my-script.zsh\").\n\
#\n\
# Two environment variables are passed to these commands. These are:\n\
#\n\
# 	$DIARY_CURRENT_ENTRY: This diary's current entry\n\
#	$DIARY_ARCHIVED_ENTRY: Only defined for the diff command, the\n\
#                          path of the file Diary is trying\n\
#                          to archive.\n\
#\n\
# The following command is run when conflicts are found. As its\n\
# name implies, it's expected to be something that deals with\n\
# conflicts, ie, times when a current has been archived and somehow\n\
# remains in the \"current file\" position (the root of the diary).\n\
# The name isn't great, and we need an example. But, you know,\n\
# maybe do something with diff, read, and mv.\n\
# conflict-diff-and-remove-command = \"./diff-and-remove.zsh\"\n\
#\n\
# The next one will be called when a new entry is made -- that is\n\
# after the previous one is archived. By default there's no action\n\
# here. I use it to enter the date and the hostname of the\n\
# computer I'm on. Here, we make a directory for this \"audio\n\
# diary\" of ours:\n\
# %s = \"\"\"\n\
# mkdir $DIARY_CURRENT_ENTRY || {\n\
#	echo \"Couldn't make $DIARY_CURRENT_ENTRY\"\n\
#	exit 1\n\
# }\n\
# exit 0\n\
# \"\"\"\n\
#\n\
# This one is the one you use to edit the current entry. By default\n\
# we open the file in your editor. Here, we use pacat to record\n\
# audio in the directory we made with new-entry-command.\n\
# edit-entry-command = \"\"\"\n\
# pacat --rate=8000 -r $DIARY_CURRENT_ENTRY/$(date +%%H_%%M_%%S).ogg --channels=1 \\\n\
#		--file-format=ogg\n\
# \"\"\"\n\
#\n\
# This is for viewing, which may well be different from editing.\n\
# Here we use mpv to play all the files in the directory we\n\
# made with new-entry-command.\n\
# view-entry-command = \"mpv $DIARY_CURRENT_ENTRY/*\"\n\
#\n\
# This is for printing the path of the current entry -- or, with --old-paths\n\
# and if running Diary has caused the last current entry \
# to be archived, the new current entry, and then all the old \
# current entries. Here you can make your own print function.\n\
# print-command = \"tree $DIARY_CURRENT_ENTRY\"",
diary_name,
start_time_str,
cfg_key_strs[UV_NEW_ENTRY]);

	fclose (f);
	return SUCC;
}

nod
static enum derr make_new_diary (char *diary_name, struct args *args) {

	char save_dir[PATH_MAX];
	if (n_fs_fexists (diary_name)) {
		fprintf (stderr, "There's already a \"%s\" in %s\n", diary_name, args->data_dir_path);
		return SUCC;
	}

	if (mk_dir (diary_name, DEFAULT_DIR_PERMISSIONS))
		return FAIL;
	getcwd (save_dir, PATH_MAX);

	if (ch_dir (diary_name))
		return FAIL;

	enum derr r = FAIL;

	if (       print_diary_cfg_template (diary_name)
			|| OPEN_IN_EDITOR ("%s %s", editor, CONFIG_FNAME))
		goto out;
	r = SUCC;
out:
	if (!r)
		return ch_dir (save_dir);
	return r;
}

nod
static char *get_archive_dir_name (void) {
	char now_str[64];
	if (!ts_str_from_time (now_str, 64, &now, DATE_FORMAT))
		WARN_RETURN (np, "%s", ts_strerror (ts_errno));
	char *r = np;
	asprintf (&r, "%s%.2s", ARCHIVE_DIR_NAME_PREFIX, now_str);
	return r;
}

nod
static enum derr set_up_new_data_dir (void) {
	FILE *f = fopen (CONFIG_FNAME, "w");
	if (!f)
		WARN_RETURN (FAIL, "Can't open %s", CONFIG_FNAME);

	fprintf (f, "\
# Here's the config file for the diary data_dir, ie, the dirs in\n\
# which all the diaries live.\n\
\n\
# This the name of the default diary, ie, the diary you'll get when\n\
# You just type \"diary\". Change it to what you want. Remove this line \
or empty the string to not make a default diary. If you don't make one \
you can make it \
later with --new.\n\
\n\
default-diary = \"diary\"\n");

	fclose (f);
	if (OPEN_IN_EDITOR ("%s %s", editor, CONFIG_FNAME))
		return FAIL;
	return SUCC;
}

nod
static enum derr exit_if_all_settings_not_got () {
	if (!settings.start.ts)
		WARN_RETURN (FAIL, "\
Each diary needs a start time in its " CONFIG_FNAME "file so we know what time to give the \
next one");
	return SUCC;
}

nod
static enum derr create_data_dir (struct args *args, struct data_dir_settings *out) {

	if (n_fs_fexists (args->new_data_dir))
		WARN_RETURN (FAIL, "%s already exists", args->new_data_dir);

	if (       mk_dir (args->new_data_dir, DEFAULT_DIR_PERMISSIONS)
			|| ch_dir (args->new_data_dir)
	        || set_up_new_data_dir ()
			|| get_data_dir_settings (args, out))
		return FAIL;
	if (!out->default_diary || !*out->default_diary)
		return SUCC;
	if (make_new_diary (out->default_diary, args))
		return FAIL;

	return SUCC;
}

nod
enum derr action_print (struct entry current_entry, struct entries *current_entries,
		struct args *args) {
	if (settings.print_cmd) {
		if (run_user_cmd (settings.print_cmd, UV_PRINT, current_entry, np,
					current_entries, args))
			return FAIL;
	} else {
		if (args->print_what & P_CURRENT_PATH)
			puts (current_entry.path);
		if (args->print_what & P_OLD_PATH) {
			EACH (_, current_entries->n, current_entries->d) {
				if (_->archived)
					puts (_->path);
			}
		}
	}
	return SUCC;
}

nod
static enum derr action_cat (struct entry current_entry) {
	if (cat (stdout, (char *[]) {current_entry.path}, 1, CAT_FLAG_SQUEEZE_BLANK))
		WARN_RETURN (FAIL, "Couldn't cat %s", current_entry.path);
	return SUCC;
}

nod
static enum derr action_view_file (struct entry current_entry,
		struct entries *current_entries, struct args *args) {
	if (settings.view_entry_cmd) {
		if (run_user_cmd (settings.view_entry_cmd, UV_VIEW_ENTRY,
					current_entry, 0, current_entries, args))
			return FAIL;
	} else {
		if (n_fs_systemf ("less %s", current_entry.path))
			WARN_RETURN (FAIL, "Couldn't run open %s in less", current_entry.path);
	}
	return SUCC;
}

nod
static enum derr action_default_edit (struct entry current_entry,
		struct entries *current_entries, struct args *args) {

	// And, finally, edit the entry.
	if (settings.edit_entry_cmd) {
		if (run_user_cmd (settings.edit_entry_cmd, UV_EDIT_ENTRY,
				current_entry, 0, current_entries, args))
			return FAIL;
	} else {
		if (OPEN_IN_EDITOR ("%s %s", editor, current_entry.path))
			return FAIL;
	}
	return SUCC;
}

nod
int main (int argc, char **argv) {
	ts_errno = 0;

	editor = getenv ("VISUAL");
	if (!editor)
		editor = getenv ("EDITOR");
	if (editor)
		editor = strdup (editor);
	// Else it's null, and we'll error if we need to.

	now = ts_time_from_ts (time (0));
	if (ts_errno) {
		WARN ("Couldn't get current time: %s", ts_strerror (ts_errno));
		exit (FAIL);
	}

	struct args args = {};
	if (set_args (argc, argv, &args))
		exit (FAIL);

	// This is unfortunately for clangd's sake. Well, maybe it's my
	// error. No: get_data_dir gets called in ARGP_KEY_END, and
	// there's an assertion there. I dunno.
	ASSERT (args.data_dir_path);

	struct data_dir_settings data_dir_settings = {};
	if (args.action == A_NEW_DATA_DIR)
		exit (create_data_dir (&args, &data_dir_settings));

	// We'll be somewhere like ~/.local/share, or wherever you set -C
	// to.
	if (ch_dir (args.data_dir_path))
		exit (FAIL);

	if (get_data_dir_settings (&args, &data_dir_settings))
		exit (FAIL);

	if (!args.diary_glob)
		args.diary_glob = data_dir_settings.default_diary;

	if (args.action == A_NEW_DIARY)
		exit (make_new_diary (args.new_diary, &args));

	if (args.action == A_LIST) {
		ls_matching_mode_t (".", S_IFDIR, 0, 1);
		exit (SUCC);
	}

	// NOTE: here I used to make a default diary. I'm not doing that
	// anymore, because now that you can specify dirs with -C, I
	// expect you to use diaries, in, eg, your projects, and you might
	// not want to call your diaries "diary".

	// This matches your glob pattern to a diary that exists in
	// ~/.local/share/diary, eg, 'diar*'. But without
	// --no-incomplete-diary, you can leave off the "*".
	char *matched_diary = match_single_diary_od (args.diary_glob,
			!(args.flags & AF_NO_ALLOW_INCOMPLETE), args.data_dir_path,
			&data_dir_settings);

	if (ch_dir (matched_diary))
		exit (FAIL);

	// Read the config file, which will be in, eg,
	// ~/.local/share/diary/my-diary/config.
	if (get_diary_settings (matched_diary, &args))
		exit (FAIL);

	// We can get settings from the root diary dir config file or the
	// particular diary's config file, so do final checks after both
	// have been parsed.
	if (exit_if_all_settings_not_got ())
		exit (FAIL);

	// The archive name is, basically, the year, like "21". But if you
	// define ARCHIVE_NAME_DIR_PREFIX in config.h, it could be, eg.
	// "archive-21".
	char *archive_dir_name = get_archive_dir_name ();
	if (!archive_dir_name)
		exit (FAIL);

	// This is basically all of the files in the diary in question
	// that match the file pattern. There should be 0 or 1. If there's
	// more, you've done something funny.
	struct entries current_entries = {};
	if (get_current_entries (matched_diary, &current_entries))
		exit (FAIL);

	// Sticks current entries into, eg, 21/whatever, returns the
	// remaining one. There will always be at least one because of
	// the if/else above.
	if (archive_files (&current_entries,
			archive_dir_name, matched_diary, strlen (matched_diary), &args))
		exit (FAIL);

	struct entry current_entry = {};
	if (get_current_entry (current_entries, matched_diary, &args, &current_entry))
		exit (FAIL);

	enum derr r;
	switch (args.action) {

	case A_PRINT:     r = action_print (current_entry, &current_entries, &args); break;
	case A_CAT_FILE:  r = action_cat (current_entry); break;
	case A_VIEW_FILE: r = action_view_file (current_entry, &current_entries, &args); break;
	default:          r = action_default_edit (current_entry, &current_entries, &args); break;
	}

	exit (r);
}
