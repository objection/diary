#pragma once

#include "lib/cat/cat.h"
#include "lib/darr/darr.h"
#include "config.h"
#include <argp.h>
#include <dirent.h>
#include <err.h>
#include <fnmatch.h>
#include <glob.h>
#include <regex.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <threads.h>
#include <unistd.h>
#include <wordexp.h>

#define nod [[nodiscard]]
enum derr {
	SUCC,
	FAIL,
};
typedef const char cchar;
#define ASSERT assert
#define SEL_1($a, ...) $a,
#define SEL_2(_, $b, ...) $b,
#define FORI($idx, $max) for (int $idx = 0; $idx < $max; $idx++)
#define EACH($elem, $n_arr, $arr) for (auto $elem = $arr; $elem < $arr + $n_arr; $elem++)
#define NUKE_IF($obj) if ($obj) { free ((void *) $obj); }
#define WARN($fmt, ...) \
	fprintf (stderr, "%s: " $fmt, program_invocation_name __VA_OPT__(,) __VA_ARGS__)
#define GOTO_OUT(...)          do { __VA_ARGS__; goto out; } while (0)
#define RETURN($val, ...)      do { __VA_ARGS__; return $val; } while (0)
#define WARN_RETURN($val, ...) do { WARN (__VA_ARGS__); return $val; } while (0)
#define WARN_GOTO_OUT(...)     do { WARN (__VA_ARGS__); goto out; } while (0)
#define np nullptr
#define LEN(...) ((ssize_t)(sizeof(__VA_ARGS__)/sizeof(typeof((__VA_ARGS__)[0]))) / ((ssize_t)(!(sizeof(__VA_ARGS__) % sizeof(typeof((__VA_ARGS__)[0]))))))
$make_arr (char *, strs);
$make_arr (char, str);
