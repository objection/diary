#pragma once

#include <stdlib.h>
#include "all.h"

nod bool fexists_and_is (char *fpath, mode_t mode);
nod char *get_xdg_data_home (char *buf, size_t nbuf);
void ls_matching_mode_t (char *dir, mode_t mode, bool print_full_path, bool
		ignore_dotfiles);
nod char *fmt (char *buf, size_t n_buf, char *fmt, ...);
nod enum derr mk_dir (cchar *path, mode_t mode);
nod enum derr ch_dir (cchar *path);
