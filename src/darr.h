
#pragma once
#include <stdbool.h>

#define MAX(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _a : _b; })

#define MIN(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _b : _a; })

#define set_arr_to_x(arr, x, n) \
	for (size_t i = 0; i < (n); i++) { \
		(arr)[i] = (x); \
	}

#define DECLARE_ARR(type, name) \
		typedef struct name##_arr { \
			type *d; \
			size_t a, n; \
		} name##_arr; \
		name##_arr create_##name##_arr(size_t init_size); \
		int push_##name##_arr(name##_arr *s, type item); \
		type pop_##name##_arr(name##_arr *s, bool error_on_stack_empty); \
		type peek_##name##_arr (name##_arr *s); \
		int insert_##name##_arr (name##_arr *s, type *what, size_t where, size_t n); \
		void remove_from_##name##_arr (name##_arr *s, size_t where, size_t n); \
		void delete_##name##_arr(name##_arr *s)

#define DEFINE_ARR(type, name) \
		name##_arr create_##name##_arr(size_t init_size) { \
			name##_arr r; \
			if (!init_size) init_size = 4; \
			r.d = malloc (sizeof(type) * init_size); \
			if (!r.d) { \
				free (r.d); \
				r.a = -1; \
			} else { \
				r.n = 0, r.a = init_size; \
			} \
			return r; \
		} \
		int push_##name##_arr(name##_arr *s, type item) {\
			type *tmp; \
			if ((*s).n >= (*s).a) { \
				tmp = realloc ((*s).d, ((*s).a * 2) * sizeof (type)); \
				if (!tmp) return -1;  \
				(*s).d = tmp; \
				(*s).a *= 2; \
			} \
			(*s).d[(*s).n++] = item; \
			return (*s).n; \
		} \
		type pop_##name##_arr(name##_arr *s, bool error_on_stack_empty) { \
			type tmp = {0}; \
			if (!(*s).n) { \
				if (error_on_stack_empty) abort(); \
				else return tmp; \
			} \
			tmp = (*s).d[--(*s).n]; \
			if ((*s).n * 2 <= (*s).a && (*s).a >= 8) { \
				(*s).a /= 2; \
				(*s).d = realloc ((*s).d, (*s).a * sizeof (type)); \
			} \
			return tmp; \
		} \
		int insert_##name##_arr (name##_arr *s, type *what, size_t where, \
				size_t n) { \
			if (where > (*s).n || where < 0) return 1; \
			if ((*s).n + n >= (*s).a) { \
				(*s).a += n + 1;  \
				(*s).a *= 2; \
				type *tmp = realloc ((*s).d, (*s).a * sizeof *(*s).d); \
				if (!tmp) return 2; \
				(*s).d = tmp; \
			} \
			memmove ((*s).d + where + n, (*s).d + where, \
					((*s).n - where) *  sizeof *(*s).d); \
			memcpy ((*s).d + where, what, n * sizeof *(*s).d); \
			(*s).n += n; \
			return 0; \
		} \
		void remove_from_##name##_arr (name##_arr *s, size_t where, size_t n) { \
			memmove ((*s).d + where, (*s).d + where + n, n); \
			(*s).n -= n; \
		} \
		void delete_##name##_arr(name##_arr *s) { \
			if ((*s).n) free ((*s).d); \
			(*s).n = (*s).a = 0; \
		} \
		type peek_##name##_arr (name##_arr *s) { \
			return (*s).d[(*s).n - 1]; \
		}
#if 0
#define arr_empty(s) (!(s).n)
#define arr_size(s) ((s).n)
#endif

