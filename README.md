# Diary

## What it's about

This is firstly primarily a CLI diary. What this means when you
invoke the program, the program will open up a diary entry in VISUAL
or EDITOR.

Every day you will get a new file to write in, and the previous
entries will be archived. Until the end of the day, repeated
invocations of diary will take you to the current entry.

I say "every day", but you can define the current-entry duration
to anything you want in your diaries' config.toml files.

And I say you get a "diary entry", but really the thing that happens
when a new entry gets made, edited or viewed can be defined by you to
be whatever you want. Eg, it could create a new directory and start
recording audio.

Diaries are directories in you Diary data dir (~/.local/share,
probably), but you should create them with "Diary --new \<name\>";
doing so will stick a default config.toml in there, will explain a bit
how you might customise Diary. Because at the core of it, Diary is a
program that archives files. And you can define hooks that decide what
gets archived and how.

## Installation

You need Meson, which you should be able to find in your package
manager. If not, it's [here](https://mesonbuild.com/Quick-guide.html).

Then, in the terminal, CD to the directory you want this
project's directory to go in and:

```
git clone --recursive 'https://gitlab.com/objection/diary'
cd diary
meson build
ninja -Cbuild install
```

## --path and --old-path: Interfacing with your editor

--path prints the path of the current diary. --old-path prints the
path of the old diary running diary has caused it to be archived, else
it prints nothing.

The point of these are to help you run diary from within your editor.
Ie, you can open, from your editor, the file returned by --path.
Assuming your editor's scriptable in any way.

--old-path will print the paths all files that have been
archived, one per line. If nothing is archived it will print nothing.
This means you can tell a) If anything's been archived at all; b)
How many files have been archived (it's one per line); and c)
Obviously, the archived files previous paths.

When I say --old-path with print "all", archived files, note
that there should never be more than one
"current diary". If there was, it'd mean there's a bug in the code or
you've changed filenames in the diary dir.

When you use both these together, the output of --path is printed
first, then that of --old-path, so it might look like this.

```

/home/me/.local/share/diary/test/test_22-03-11T23:00:00.txt
/home/me/.local/share/diary/test/test_22-01-11T23:00:00.txt
/home/me/.local/share/diary/test/test_22-02-11T23:00:00.txt

```

Here's a bit of not particularly robust Vimscript to demonstrate how
you might use these. I've commented it a lot, so might get it even if
you don't know Vimscript.

```vim

func! Diary (which, mods, arbitrary_args, curwin = 0)

	" Run diary with --old-path and --path. Vim's builtin systemlist
	" function returns a list where each element is a line.
	let diary_out =
			\ systemlist ('diary '..a:arbitrary_args..
			\ ' --old-path --path '..a:which)

	" If diary failed ...
	if v:shell_error != 0

		" ... Print the error message. Vim's system functions
		" by default collect both stdout and stdin, hence it'll be in
		" this string.
		echo diary_out->join("\n")
	else

		" Else we've got proper output. First is new diary path.
		let diary_path = diary_out[0]

		" Edit the new diary. The branch here is to do with whether
		" you want to open the diary in a new split or not.
		if a:curwin != 0
			exe 'e' diary_path
		else
			exe a:mods..' split '..diary_path
		endif

		" The rest of the list consists old diary (pre-archive) paths.
		let diary_old_paths = diary_out[1:-1]

		" Delete those old buffers without complaining if the buffers
		" don't exist.
		if len (diary_old_paths)
			silent! exe 'bwipe' diary_old_paths->join()
		endif

	endif
endfunc


" This is a Vim command. You use it like this: "Diary", or "Diary
" test", "Diary other-diary". It just calls the function defined
" above with the arg you've given it, and these "<q-mods>". Who
" care what those are.
command! -nargs=? Diary {
	call Diary(<q-args>, <q-mods>)
}

```

## Note on Grep, Vim, filenames

Diary's default naming scheme makes it ungreppable from within Vim.
The reason is diary uses colons in its filenames (eg:
diary_22-03-25T23:00:00.txt), and grep uses colons to separate
filenames, lines and text in its output, which Vim relies on to parse
grep's output.

If you want to avoid this, change HOURS_SEP in config.h to some other
character, say a dot, so that the above becomes
diary_22-03-25T23.00.00.txt.

Downside of that, I suspect, is you might forget to make your change
one day when you compile. Not that it'd do much more than make your
diary entries' names inconsistent.

Another method would be to put this in your vimrc.

```vim9
def! Grep(cmdline: string)

	var lines = systemlist('grep -ZsHn ' .. expandcmd(cmdline))

	var qf_list = []
	for line in lines
        var sundered    = split(line, "\n")
        var filename   	= sundered[0]
        var lnum        = matchstrpos(sundered[1], '\d\+')
        var text        = sundered[1][lnum[2] + 1 : -1]
		call add(qf_list,
			\ {filename: filename, lnum: str2nr(lnum[0]), text: text})
	endfor

	call setqflist(qf_list, 'r')
	copen
enddef

command -nargs=+ Grep grep.Grep(<q-args>)

```

